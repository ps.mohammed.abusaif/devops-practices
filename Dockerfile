FROM openjdk:8

MAINTAINER ABUSAIF
EXPOSE 8090

RUN mkdir /Devops
COPY target/assignment-*.jar /Devops/app.jar
WORKDIR /Devops/

ENTRYPOINT ./checkDbReadiness.sh
