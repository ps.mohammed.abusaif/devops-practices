echo "To check the progress 'docker logs -f oracle'"
    while [ "$db_ready" = "" ]; do
        #export db_ready=$(docker logs cont_mysql_database 2>&1 | grep -o "mysqld: ready for connections.");
        export db_ready=$(docker logs abusaif-stack_db* 2>&1 | grep -o "mysqld: ready for connections.");
        x=$((x+1));echo -ne "\r${BAR:0:$x}";
	    sleep 3;
done
